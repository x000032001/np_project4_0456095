#pragma once
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include "Logger.h"

struct Setting {
	char mode;
	uint32_t ip;
	uint16_t port;

	Setting() : mode(0), ip(0), port(0) {};
};

class Rules {
public:
	int Init(const char* conf_file);
	Setting set[100];	
};
