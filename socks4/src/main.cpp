#include "common.h"
#include "TCPServer.h"
#include "Worker.h"
#include "Rules.h"

Rules rule;

int main(int argc, char **argv)
{
	signal(SIGCHLD, SIG_IGN);
    SET_LOG_LEVEL(DEBUG);

    int port = PORT;

    for( int i = 1; i < argc ; ++i ) {
        int res = strtol(argv[i],NULL,10);
        if( res != 0 ) {
            port = res;
            continue;
        }
    }

	const char conf_file[] = ETC_DIR "/socks.conf";
	struct stat sb;
	if (stat(conf_file, &sb) == 0 && S_ISREG(sb.st_mode)) {
		if(0 > rule.Init(conf_file))
			slogf(ERROR, "Reading rule file error\n");
	}

	int sockfd = tcp_server(port);
	if(sockfd < 0)
		slogf(ERROR, "build listening socket failed\n");

    while(1) {
        int connfd = accept_connection(sockfd);
		pid_t pid;
        if(pid = fork()) {
            slogf(INFO, "fork() = %d\n",pid);
            close(connfd);
        } else {
			close(sockfd);
            serve(connfd);
            break;
        }
    }

    return 0;
}
