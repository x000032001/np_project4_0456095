#include "Rules.h"

int Rules::Init(const char* conf_file)
{
	FILE* file = fopen(conf_file, "r");
	if(!file) {
		slogf(DEBUG, "file not found %s\n",conf_file);
		return -1;
	}

	char buff[1024] = {};
	int count = 0;
	while(fgets(buff, 1024, file)) {
		if(buff[0]=='#')	continue;
		char useless[10] = {};
		char mode[5] = {};
		char src_ip[30] = {};
		char src_port[10] = {};
		char dst_ip[30] = {};
		char dst_port[10] = {};
		if(sscanf(buff, "%s %s %s %s %s %s",useless, mode, src_ip, src_port, dst_ip, dst_port)<=0) {
			continue;
		}
		slogf(INFO, "mode = %s\tdst_ip = %s\tdst_port = %s\n",mode, dst_ip, dst_port);

		if(mode[0] == 'c')
			set[count].mode = 'c';
		else if(mode[0] == 'b')
			set[count].mode = 'b';
		else {
			slogf(WARN, "mode error\n");
			continue;
		}

		char ip[4][10] = {};
		if(dst_ip[0] != '-' &&
			0>=sscanf(dst_ip, "%[^.].%[^.].%[^.].%[^.]",ip[0], ip[1], ip[2], ip[3])) {
			slogf(WARN, "ip error\n");
			continue;
		}
		//slogf(DEBUG, "%s %s %s %s\n",ip[0], ip[1], ip[2], ip[3]);
		set[count].ip = 0;
		for(int i = 0; i < 4; ++i) {
			set[count].ip <<= 8;
			if(ip[i][0] == '*')	continue;
			uint8_t n = atoi(ip[i]);
			set[count].ip |= n;
		}
		//slogf(DEBUG, "ip = %x\n",set[count].ip);

		uint16_t port = 0;
		if(dst_port[0] != '-' &&
			0 >= sscanf(dst_port, "%hu", &port)) {
			slogf(WARN, "port error\n");
			continue;
		}

		set[count].port = ((port % 256)<<8) | (port / 256);

		//slogf(DEBUG, "port = %hu\n", port);

		count++;
		if(count >= 100) {
			slogf(WARN, "rules exceed limit\n");
			break;
		}
	}

	return 0;
}
