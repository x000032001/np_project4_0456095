#pragma once

#include "common.h"

enum Methods {
    GET,
    POST
};

class Request {
public:
    int method;
    string uri;
    string version;
    map<string, string> headers;
    string content;
    void Print() {
        printf("%d %s %s\n",method, uri.c_str(), version.c_str());
        for(auto it : headers)
            printf("[%s]=%s\n",it.first.c_str(), it.second.c_str());
        printf("Body: %s\n",content.c_str());
    }
};

int TryParseRequest(string& buffer, Request &req);
