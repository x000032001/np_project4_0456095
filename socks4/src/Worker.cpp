#include "Worker.h"

extern Rules rule;

int get_conn(uint32_t ip, char *domain, uint16_t port)
{
    struct sockaddr_in s_addr;
    memset(&s_addr, 0, sizeof(s_addr));

	if(domain != NULL) {
		struct hostent *hptr;  
		if ((hptr = gethostbyname(domain)) == NULL)   
		{   
			return -1;
		}  

		memcpy(&s_addr.sin_addr, hptr->h_addr_list[0], hptr->h_length);
		s_addr.sin_family = hptr->h_addrtype;
		s_addr.sin_port = port;
	} else {
		s_addr.sin_family = AF_INET;
		s_addr.sin_port = port;
		s_addr.sin_addr.s_addr = ip;
	}

	int c_fd;
    if ((c_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("create socket fail.\n");
		return -1;
    }

    int ret = connect(c_fd, (struct sockaddr*)&s_addr, sizeof(s_addr));
    if (ret < 0) {
		perror("connect remote server fail.\n");
		printf("%d\n", errno);
		return -1;
    }

	return c_fd;
}

void serve(int connfd)
{
	char buff[2048] = {};
	int rc = read(connfd, buff, 2048);

	/* get request info */
	uint8_t version = buff[0];
	uint8_t code = buff[1];
	uint16_t dst_port = buff[3] << 8 | buff[2];
	uint32_t dst_ip = ((uint32_t)buff[7] << 24) & (0xFF<<24) | ((uint32_t)buff[6] << 16) & (0xFF<<16) | ((uint32_t)buff[5] << 8)&(0xFF<<8) | ((uint32_t)buff[4])&0xFF;
	char* user_id = buff+8;
	char* domain_name = NULL;
	if(buff[4] == 0 && buff[5] == 0 && buff[6] == 0) {
		domain_name = user_id;
		while(*domain_name++);

		domain_name++;
	}
	if(version != 4)exit(0);

	int permit = 0;
	for(int i = 0; i < 100 && !permit; ++i) {
		if(rule.set[i].mode != (code == 1 ? 'c' : 'b'))
			continue;

		if(dst_ip & rule.set[i].ip != rule.set[i].ip)
			continue;

		if(rule.set[i].port && rule.set[i].port == dst_port)
			permit = 1;
	}

	slogf(INFO, "Request: \nVN=%hhu\n"
				"CD=%hhu\n"
				"Port=%hd\n"
				"IP=%hhu.%hhu.%hhu.%hhu\n"
				"user_id=%s\n"
				"domain_name=%s\n"
				"Permit? = %d\n",
				version, code, ntohs(dst_port), buff[4], buff[5], buff[6], buff[7],
				user_id, domain_name, permit
			);

	if(code == 1) { // CONNECT MODE

		slogf(INFO, "connecting\n");
		int dst_fd = get_conn(dst_ip, domain_name, dst_port);

		uint16_t port = dst_port;
		uint32_t ip = code == 1 ? dst_ip : 0;

		uint8_t result[8] = {};
		result[0] = 0;
		result[1] = dst_fd >= 0 && permit == 1 ? 90 : 91;
		result[2] = port / 256;
		result[3] = port % 256;
		result[4] = ip >> 24;
		result[5] = (ip >> 16) & 0xFF;
		result[6] = (ip >> 8) & 0xFF;
		result[7] = ip & 0xFF;

		write(connfd, result, 8);

		if(!permit)
			exit(0);

		slogf(INFO, "setup ok\n");

		if(dst_fd < 0)
			exit(0);

		while(1) {
			pollfd poll_fd[2] = {
				{ connfd, POLLIN } ,
				{ dst_fd, POLLIN }
			};

			int rc = poll(poll_fd, 2, -1);
			if(rc <= 0)
				break;
			for(int i = 0; i < 2; ++i) {
				if(!poll_fd[i].revents)
					continue;
				if(poll_fd[i].revents != POLLIN)
					exit(0);
				while(1) {
					char buff[1024] = {};
					int len = recv(poll_fd[i].fd, buff, 1024, MSG_DONTWAIT);
					if((len < 0 && errno == EAGAIN))
						break;
					if(len == 0)
						exit(0);
					int len2 = send(poll_fd[i^1].fd, buff, len, 0);
					if(len2 < 0)
						exit(0);
				}
			}
		}
	} else { // BIND MODE

		slogf(INFO, "binding\n");
		int sockfd = -1;
		uint16_t port = dst_port;
		struct sockaddr_in sAddr;
		socklen_t sock_len = sizeof(sAddr);

		if(!permit) goto DONE;
		if (0 > (sockfd = socket(AF_INET, SOCK_STREAM, 0))) {
			slogf(ERROR, "socket() %s\n", strerror(errno));
		}

		bzero((char*)&sAddr, sizeof(sAddr));
		sAddr.sin_family = AF_INET;
		sAddr.sin_addr.s_addr = INADDR_ANY;
		sAddr.sin_port = INADDR_ANY;

		setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sAddr, sizeof(sAddr));

		if (0 > bind(sockfd, (struct sockaddr*)&sAddr, sizeof(sAddr))) {
			slogf(WARN, "bind() %s\n", strerror(errno));
			sockfd = -1;
			goto DONE;
		}

		if (getsockname(sockfd, (struct sockaddr *)&sAddr, &sock_len) < 0){
			    cout << "failed to get hostname with errno: "<< errno << endl;
				    exit(1);
		}

		port = sAddr.sin_port;

		if (0 > listen(sockfd, 5)) {
			slogf(WARN, "listen() %s\n", strerror(errno));
			sockfd = -1;
			goto DONE;
		}

		slogf(INFO, "Listen Ok at Port [%d]\n",ntohs(port));

DONE:

		uint32_t ip = code == 1 ? dst_ip : 0;

		uint8_t result[8] = {};
		result[0] = 0;
		result[1] = sockfd >= 0 && permit == 1 ? 90 : 91;
		result[2] = port % 256;
		result[3] = port / 256;
		result[4] = ip >> 24;
		result[5] = (ip >> 16) & 0xFF;
		result[6] = (ip >> 8) & 0xFF;
		result[7] = ip & 0xFF;

		write(connfd, result, 8);
		if(!permit)exit(0);
		if(sockfd < 0)exit(0);

		slogf(INFO, "accepting...\n");
		int dst_fd = -1;
		dst_fd = accept_connection(sockfd);

		result[1] = dst_fd >= 0 ? 90 : 91;

		write(connfd, result, 8);

		slogf(INFO, "setup ok\n");

		close(sockfd);

		if(dst_fd < 0)
			exit(0);

		while(1) {
			pollfd poll_fd[2] = {
				{ connfd, POLLIN } ,
				{ dst_fd, POLLIN }
			};

			int rc = poll(poll_fd, 2, -1);
			if(rc <= 0)
				break;
			for(int i = 0; i < 2; ++i) {
				if(!poll_fd[i].revents)
					continue;
				if(poll_fd[i].revents != POLLIN)
					exit(0);
				while(1) {
					char buff[1024] = {};
					int len = recv(poll_fd[i].fd, buff, 1024, MSG_DONTWAIT);
					if((len < 0 && errno == EAGAIN))
						break;
					if(len == 0)
						exit(0);
					int len2 = send(poll_fd[i^1].fd, buff, len, 0);
					if(len2 < 0)
						exit(0);
				}
			}
		}

	}
	

    exit(0);
}

vector<string> split(const string &source, const string &delim)
{
    vector<string> ans;
    size_t begin_pos=0, end_pos=source.find(delim); 
    while(end_pos!=string::npos) {
        ans.push_back(source.substr(begin_pos, end_pos-begin_pos)); 
        begin_pos = end_pos+delim.size();
        end_pos = source.find(delim, begin_pos);  
    }
    ans.push_back(source.substr(begin_pos, end_pos-begin_pos));  
    return ans;
}

bool hasEnding (std::string const &fullString, std::string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}
