#pragma once

#include <string>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <poll.h>
 

#include "Logger.h"
#include "Request.h"
#include "TCPServer.h"
#include "Rules.h"

using namespace std;

void serve(int connfd);
vector<string> split(const string &source, const string &delim);
bool hasEnding (std::string const &fullString, std::string const &ending);
