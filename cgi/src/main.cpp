#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/select.h>
#include <error.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "Logger.h"
#include "HtmlWrapper.h"
#include "util.h"
#include "coroutine.h"

#define NO 0
#define WRITE 1
#define READ 4
#define CONNECT 2
#define CLOSE 3

#define ERR -1
#define DONE -2

using namespace std;

int lastRegisteredEvent[6] = {};
int reachedEvent[6] = {};

struct args {
    args() { connfd = 0; id = 0; file = NULL; }
	int connfd;
    int id;
    FILE *file;
	uint32_t ip;
	uint16_t port;
};

int yield_read(schedule *S,int id, int connfd, char *buff, int size) {
	/* socket err event from epoll */
	if(reachedEvent[id] == CLOSE) {
		slogf(DEBUG, "%d closed\n", id);
		return ERR;
	}

	/* call read first time */
	if(reachedEvent[id] == NO) {
		/* simply set event and yield */
		lastRegisteredEvent[id] = READ;
		/* after resumed, we should get the reached event */
		coroutine_yield(S);
	}

	if(reachedEvent[id] != READ) {
		slogf(WARN, "reached event should be READ but get %d\n",reachedEvent[id]);
		return ERR;
	}

	int len = read(connfd, buff, size);

	if(len < 0) {
		if(errno == EAGAIN) {
			/* when we call `yield_read` again, it'll yield due to no event reaches */
			reachedEvent[id] = NO;
			return DONE;
		}
		slogf(WARN, "read() error: %s\n",strerror(errno));
		return ERR;
	}  
	else if(len == 0) { /* server side closed normally */
		return ERR;	
	}

	return len;
}

int yield_write(schedule *S,int id, int connfd, char *buff, int size) {

	int len = size;
	for( char *ptr = buff ; len ; ) {
		int rc = write(connfd, ptr, len);
		if(rc < 0) {
			/* write buff full, wait for writable event */
			if(errno == EAGAIN) {
				lastRegisteredEvent[id] = WRITE;
				coroutine_yield(S);
				if(reachedEvent[id] == WRITE) {
					continue;
				} else if(reachedEvent[id] == CLOSE) {
					return ERR;
				} else {
					slogf(WARN, "reached event should be WRITE but got %d\n",reachedEvent[id]);
					return ERR;
				}
			} 
		}

		ptr += rc;
		len -= rc;
	}

	return size;
}

void serve(schedule *S, void *ud) {
    args *arg = (args*)ud;
    int connfd = arg->connfd;
    int id = arg->id;
    FILE* file = arg->file;

	/* negotiate with proxy */
	char buff[2048] = {};
	buff[0] = 4;
	buff[1] = 1;
	buff[3] = (arg->port >> 8) & 0xFF;
	buff[2] = arg->port & 0xFF;
	buff[7] = (arg->ip >> 24) & 0xFF;
	buff[6] = (arg->ip >> 16) & 0xFF;
	buff[5] = (arg->ip >> 8) & 0xFF;
	buff[4] = arg->ip & 0xFF;
	buff[8] = 0;
	write(connfd, buff, 8);
	
	char reply[8] = {};
	read(connfd, reply, 8);
	if(reply[1] != 90) {
		const char msg[] = "Proxy refused!";
		HtmlWrapper::Print(id-1, msg, sizeof(msg)-1, false);
		return;
	}
	reachedEvent[id] = NO;
	/* negotiate with proxy */

    bool eof = false;
    bool hasPrompt = false;
    while(1) {
        hasPrompt = false;
		for( ;; ) {
			char buff[1024] = {};
			int len = yield_read(S, id, connfd, buff, 1024);
			if(len == DONE)
				break;
			if(len == ERR)
				return;
			HtmlWrapper::Print(id-1, buff, len, false);
			for(int i = 0; i+1 < len && !hasPrompt ; ++i) {
				if(buff[i]=='%' && buff[i+1] == ' ')
					hasPrompt = true;
			}

			if(len != 1024)
				break;
		}

        for(; !eof && hasPrompt ;) {
            char buff[1024] = {};
            char *rc = fgets(buff, 1023, file);
            if(rc == NULL) { // EOF
                eof = true;
                break;
            }

            int len = strlen(buff);

            HtmlWrapper::Print(id-1, buff, len, true);

			yield_write(S, id, connfd, buff, len);

            if(len != 1022) // contains new line char
                break;
        }
    }
}

int main()
{
    map<string, string> querys = queryExtract();

    ////////////////////////////////////////
    /*           epoll variables          */
    int epoll_fd = epoll_create(10);
    struct epoll_event event;
    event.events = EPOLLIN | EPOLLOUT | EPOLLET;
    ////////////////////////////////////////

    ////////////////////////////////////////
    /*        coroutine variable          */
    struct schedule *S = coroutine_open();
    args info[6];
    ////////////////////////////////////////

    ////////////////////////////////////////
    /*        connection setup            */
    int conn_count = 0;
    string header_msg[5] = {};
    for(int i = 1; i <= 5; ++i) {
		const string host = "h"+to_string(i);
		const string port = "p"+to_string(i);
		const string file_name = "f"+to_string(i);
		const string proxy_host = "sh" + to_string(i);
		const string proxy_port = "sp" + to_string(i);

        int rc = getConnSocket(querys[proxy_host], querys[proxy_port]);
        FILE* file = fopen(querys[file_name].c_str(), "r");

        if(rc < 0) {
            if(querys[host] =="" || querys[port] == "")
                header_msg[i-1] = "Missing setting";
            else
                header_msg[i-1] = "[FAIL] Connect to " + querys[host] + "/" + querys[port];

            if(file)    fclose(file);
            continue;
        }

        if(!file) {
            header_msg[i-1] = "[FAIL] Open file " + querys[host];
            slogf(WARN, "%d open file failed\n",i);
            close(rc);
            continue;
        }

        info[i].connfd = rc;
        info[i].file = file;
        info[i].id = i;
		info[i].port = htons(stol(querys[port]));

		struct sockaddr_in s_addr;
		struct hostent *hptr;  
		if ((hptr = gethostbyname(querys[host].c_str())) == NULL)   
		{   
			slogf(WARN, "gethostbyname error %s\n",strerror(errno));
			continue;
		}  
		memcpy(&s_addr.sin_addr, hptr->h_addr_list[0], hptr->h_length);

		info[i].ip = s_addr.sin_addr.s_addr;

        event.data.fd = i;
        epoll_ctl(epoll_fd, EPOLL_CTL_ADD, rc, &event);
        conn_count++;

        header_msg[i-1] = querys[host] + "/" + querys[port];
    }

    HtmlWrapper::Init(header_msg);
    slogf(INFO, "Setup Ok!\n");
    ////////////////////////////////////////

    ////////////////////////////////////////
    /*        coroutine setup             */
    int cr[6] = {};
    for(int i = 1; i <= 5; ++i) {
        if(info[i].connfd != 0) {
            cr[i] = coroutine_new(S, serve, &info[i]);
        }
    } 
    ////////////////////////////////////////
    
    slogf(INFO, "Start main loop\n");

    while (conn_count != 0) {
        struct epoll_event events[10];
        int rc = epoll_wait(epoll_fd, events, 10, -1); // -1 for block until anyone ready
        if (rc < 0) {
            printf("epoll_wait err\n");
            exit(0);
        }
        for (int i = 0; i < rc; ++i) {
            int id = events[i].data.fd;
            /* handle disconnected events */
            if (events[i].events & EPOLLERR || events[i].events & EPOLLHUP || events[i].events & EPOLLRDHUP) {
				slogf(DEBUG, "EPOLLERR %d\n",id);
                conn_count--;
                reachedEvent[id] = CLOSE;

                if(coroutine_status(S, cr[id]) != COROUTINE_DEAD) {
                    coroutine_resume(S, cr[id]);
                } else {
                    slogf(ERROR, "resume a dead coroutine %d\n",id);
                }

                continue;
            }

            /* handle readable*/
            if (events[i].events & EPOLLIN) {
				slogf(DEBUG, "EPOLLIN %d\n",id);
                if(lastRegisteredEvent[id] == READ || lastRegisteredEvent[id] == NO) {
                    //slogf(ERROR, "registered event not matches reached event for cr %d\n",id);
                    reachedEvent[id] = READ;

                    if(coroutine_status(S, cr[id]) != COROUTINE_DEAD) {
						slogf(DEBUG, "resume %d\n",id);
                        coroutine_resume(S, cr[id]);
                        if(lastRegisteredEvent[id] == WRITE) {
                            event.events = EPOLLIN | EPOLLOUT | EPOLLET;
                            epoll_ctl(epoll_fd, EPOLL_CTL_MOD, info[id].connfd, &event);
                        }
                        if(coroutine_status(S, cr[id]) == COROUTINE_DEAD)
                            conn_count--;
                    } else {
                        slogf(ERROR, "resume a dead coroutine %d\n",id);
                    }
                }
            }


            /* 觸發情況
             * 1. 連線第一次建立
             * 2. 緩衝區由滿 -> 可寫
             * 3. 重新被加入epoll_fd
             * 總之就是writable
             */
            if (events[i].events & EPOLLOUT) {
				slogf(DEBUG, "EPOLLOUT %d\n",id);
                /* connected */
                if(lastRegisteredEvent[id] == NO) {
                    reachedEvent[id] = CONNECT;
                } else {
                    if(lastRegisteredEvent[id] != WRITE) 
                        continue;
                        //slogf(ERROR, "registered event %d not matches reached event EPOLLOUT for cr %d\n",lastRegisteredEvent[id],id);
                    reachedEvent[id] = WRITE;
                }

                if(coroutine_status(S, cr[id]) != COROUTINE_DEAD) {
                    slogf(INFO, "coroutine resume %d [%d]\n",id,reachedEvent[id]);
                    coroutine_resume(S, cr[id]);
                    if(coroutine_status(S, cr[id]) == COROUTINE_DEAD)
                        conn_count--;
                } else {
                    slogf(ERROR, "resume a dead coroutine %d\n",id);
                }
            }

        }
    }

    HtmlWrapper::Final();
    coroutine_close(S);

    return 0;
}

