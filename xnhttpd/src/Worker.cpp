#include "Worker.h"

void serve(const string root, int connfd)
{
    string req_str;
    // take request
    Request req;
    while(1) {
        char buff[513] = {};
        int rc = recv(connfd, buff, 512, MSG_DONTWAIT);
        if(rc < 0) {
            if(errno == EAGAIN) {
				if(req_str.size() != 0)
					break;
				else
					continue;
			}
            slogf(ERROR, "Read error %d\n", rc);
        }

        req_str += buff;

        if(rc == 0 && req_str.size() != 0)
            break;
    }

    if(0 != TryParseRequest(req_str, req)) {
		dprintf(connfd, "HTTP/1.0 404 Not Found\r\n");
		dprintf(connfd, "Parse request failed\r\n");
        slogf(ERROR, "Parse request failed\n");
	}

    // find query string
    auto res = split(req.uri, "?");
    string query_str;
    if(res.size() == 2) {
        query_str = res[1];
    }

    // is .cgi
    if(!hasEnding(res[0], ".cgi")) {
        string file = root + res[0];
        int rc = open(file.c_str(), O_RDONLY);
        if(rc < 0) {
			dprintf(connfd, "HTTP/1.1 404 Not Found\r\n\r\n");
            dprintf(connfd, "not cgi file and not found\n");
            slogf(ERROR, "not .cgi\n");
        }
        struct stat stat_buf;
        fstat(rc, &stat_buf);
        dprintf(connfd, "HTTP/1.1 200 OK\r\n\r\n");
        sendfile(connfd, rc, NULL, stat_buf.st_size);
        close(connfd);
        exit(0);
    }

    string file = root + res[0];
    struct stat sb;
    if (stat(file.c_str(), &sb) == 0 && S_ISREG(sb.st_mode)) {
        clearenv();
        setenv("QUERY_STRING", query_str.c_str(), 3);
        setenv("CONTENT_LENGTH", "0", 3);
        setenv("REQUEST_METHOD", "GET", 3);
        setenv("SCRIPT_NAME", file.c_str(), 3);
        setenv("AUTH_TYPE", "", 3);
        setenv("REMOTE_USER", "", 3);
        setenv("REMOTE_IDENT", "", 3);

        //dprintf(connfd, "HTTP/1.1 200 OK\r\n\r\n");

        dup2(connfd, 0);
        dup2(connfd, 1);
        //dup2(connfd, 2);
        execl(file.c_str(), res[0].c_str());
    } else {
        dprintf(connfd, "HTTP/1.1 404 Not Found\r\n\r\n");
		slogf(INFO, "File loc: %s\n", file.c_str());
    }

    exit(0);
}

vector<string> split(const string &source, const string &delim)
{
    vector<string> ans;
    size_t begin_pos=0, end_pos=source.find(delim); 
    while(end_pos!=string::npos) {
        ans.push_back(source.substr(begin_pos, end_pos-begin_pos)); 
        begin_pos = end_pos+delim.size();
        end_pos = source.find(delim, begin_pos);  
    }
    ans.push_back(source.substr(begin_pos, end_pos-begin_pos));  
    return ans;
}

bool hasEnding (std::string const &fullString, std::string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}
