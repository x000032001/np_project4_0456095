#include "TCPServer.h"

int tcp_server(int port)
{
	int sockfd = -1;
    if (0 > (sockfd = socket(AF_INET, SOCK_STREAM, 0))) {
        slogf(ERROR, "socket() %s\n", strerror(errno));
    }

    struct sockaddr_in sAddr;

    bzero((char*)&sAddr, sizeof(sAddr));
    sAddr.sin_family = AF_INET;
    sAddr.sin_addr.s_addr = INADDR_ANY;
    sAddr.sin_port = htons(port);

    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sAddr, sizeof(sAddr));

    if (0 > bind(sockfd, (struct sockaddr*)&sAddr, sizeof(sAddr))) {
        slogf(WARN, "bind() %s\n", strerror(errno));
		return -1;
    }

    if (0 > listen(sockfd, 5)) {
        slogf(WARN, "listen() %s\n", strerror(errno));
		return -1;
    }

    slogf(INFO, "Listen Ok at Port [%d]\n",port);

    return sockfd;
}

int accept_connection(int sockfd)
{
	struct sockaddr_in cAddr;
	socklen_t len = sizeof(cAddr);
	bzero((char*)&cAddr, sizeof(cAddr));

	int connfd = accept(sockfd, (struct sockaddr*)&cAddr, &len);
	if (connfd < 0) {
		slogf(WARN, "accept() %s\n", strerror(errno));
		return -1;
	}

	slogf(DEBUG, "Connection %s/%d\n", inet_ntoa(cAddr.sin_addr), ntohs(cAddr.sin_port));
	return connfd;
}

