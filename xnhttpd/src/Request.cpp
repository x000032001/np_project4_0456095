#include "Request.h"

int TryParseRequest(string& buffer, Request &req)
{
    if(buffer.size() == 0){
		slogf(WARN, "buffer is 0\n");
	   	return -1;
	}

    string bak = buffer;
    stringstream ss(bak);

    string line;
    if(!getline(ss, line)) {
        slogf(DEBUG, "Fail on get first line\n");
        return -2;
    }

    slogf(DEBUG, "%s\n",line.c_str());
    char m[64]={}, uri[1024]={}, ver[64]={};
    if(sscanf(line.c_str(), "%64s %1024s %64s",m,uri,ver)) {
        if(!strcmp(m, "GET"))
            req.method = GET;
        else if(!strcmp(m, "POST"))
            req.method = POST;
        else
            slogf(WARN, "Unsupport Method %s\n",m);
        req.uri = uri;
        req.version = ver;
    } else {
        slogf(WARN, "Parse first line fail\n");
    }

    while(getline(ss, line)) {
        if(line.size() < 3) break;

        int size = line.size();
        char key[128] = {};
        char val[size];
        memset(val, 0, size);
        if(sscanf(line.c_str(), "%[^:]: %s", key, val)) {
            char *ptr = key;
            while(*ptr) {
                *ptr = tolower(*ptr);
                ptr++;
            }

            ptr = val;
            while(*ptr) {
                *ptr = tolower(*ptr);
                ptr++;
            }

            req.headers[key] = val;
        } else {
            slogf(WARN, "Parse Header Fail: %d %s\n",size, line.c_str());
        }
    }

    if(req.headers.find("content-length") != req.headers.end()) {
        int length = stol(req.headers["content-length"]);
        if(length > 0) {
            char buf[length+1];
            ss.read(buf, length);
            buf[length] = 0;
            req.content = buf;
        } else {
            slogf(WARN, "Parse Length Fail\n");
        }
    }

    int size = ss.tellg();
    if(size > 0)buffer = buffer.substr(size);

    //req.Print();

    return 0;
}
