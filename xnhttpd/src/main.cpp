#include "common.h"
#include "TCPServer.h"
#include "Worker.h"

int main(int argc, char **argv)
{
	signal(SIGCHLD, SIG_IGN);
    SET_LOG_LEVEL(DEBUG);

    int port = PORT;
    string webroot = WEB_ROOT;

    for( int i = 1; i < argc ; ++i ) {
        int res = strtol(argv[i],NULL,10);
        if( res != 0 ) {
            port = res;
            continue;
        }

        struct stat sb;
        if (stat(argv[i], &sb) == 0 && S_ISDIR(sb.st_mode))
            webroot = string(argv[i]);
    }

	int sockfd = tcp_server(port);
	if(sockfd < 0)
		slogf(ERROR, "build listening socket failed\n");
	slogf(INFO, "webroot: %s\n",webroot.c_str());

    while(1) {
        int connfd = accept_connection(sockfd);
		pid_t pid;
        if(pid = fork()) {
            slogf(INFO, "fork() = %d\n",pid);
            close(connfd);
        } else {
            serve(webroot, connfd);
            break;
        }
    }

    return 0;
}
